#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <limits.h>
#include <stdint.h>

#ifndef BARBRE_H_
#define BARBRE_H_

typedef struct element {
    int clef; //information stockée
    struct page* pg; //pointeur sur une page
} element;

typedef struct page {
    int ordre; //ordre de la page / max key = order - 1
    int nb; //compteur de clés dans la page
    element* tab;
} page;

page* new_page(int ordre);
page* inserer(page* b_arbre, int clef);
int position(page* pg, int clef);
int placer(page* pg, element* cell);;
page* inserer_case(page* b_arbre, element* cell, int depth);

page* search(page* b_arbre, int clef);
void display_RGD(page* b_arbre, int depth);
void display_GRD(page* b_arbre);

void free_b_arbre(page* b_arbre);

#endif
CC=gcc
CFLAGS=-Wall -Wextra -std=c11 -g -fsanitize=leak -fsanitize=undefined  -fsanitize=address
LDFLAGS=-lm
LIBS= -lSDL2 -lm

FOLDER = .
SOURCES = $(wildcard $(FOLDER)/*.c)
OBJECTS = $(SOURCES:.c=.o)

TARGET = test_b_arbre

$(TARGET) : $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LIBS)

.PHONY: clean 

clean:
	rm -f $(TARGET) $(OBJECTS)

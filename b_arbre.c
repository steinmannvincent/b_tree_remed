#include "b_arbre.h"

page* new_page(int ordre){
    page* new_b = calloc(1,sizeof(page));
    new_b->ordre = ordre;
    new_b->nb = 0;
    new_b->tab = malloc(sizeof(element)*(2 * ordre +2)); //Need 6 cases (4 value, 1 for 0 and 1 for separation)
    for(int i = 0; i < ( 2 * ordre +2); i++){
        new_b->tab[i].pg = NULL; //Set the malloc to nulls
    }
    return new_b;
}

page* inserer(page* b_arbre, int clef){
   if(search(b_arbre, clef) == NULL){
        element new_cell;
        new_cell.clef = clef;
        new_cell.pg = NULL;
        page* new_page = inserer_case(b_arbre, &new_cell, 0); //In the stack
        //New cell only needed to give data to tab so tab has value and new_cell is not needed
        return new_page; 
   }
   return b_arbre; 
}

int position(page* pg, int clef){
    if(pg != NULL){
        if(pg->nb == 0){ //If empty place case 1
            return 1;
        }
        if(clef < pg->tab[1].clef){ //Doesn't count cell 0
            if(pg->tab[0].pg != NULL){ //If there is already a page, go inside
                return 0;
            }
            return 1;
        }
        int cpt = 0;
        while(cpt < pg->nb && clef > pg->tab[cpt+1].clef){ //Not bigger than max and bigger as next elem
            cpt++;
        }
        if(pg->tab[cpt].pg == NULL){
            return cpt + 1;
        }
        return cpt;
    }
    return 0;    
}

int placer(page* pg, element* cell){
    if(search(pg,cell->clef) != NULL){ //There already is the value
        return 0;
    }
    int pos;
    for(pos = 1; pos <= pg->nb; pos++){ //Goes through all cell
        if(pg->tab[pos].clef > cell->clef){ 
            break; //Current value bigger than needed
        }
    }
    for(int i = pg->nb; i >= pos; i--){
        pg->tab[i+1] = pg->tab[i]; //Put one case before
    }
    pg->tab[pos] = *cell;
    pg->nb++;

    //Separation
    if(pg->nb > 2* pg->ordre){ //max nb = order -1
        page* right = new_page(pg->ordre); 
        for(int i = pg->ordre + 2; i <= pg->nb; i++){ //ordre +2 because center managed in inserer_case and left stay
            placer(right, &pg->tab[i]);
        }
        //Changes val afer separation
        pg->nb = pg->ordre;
        cell->clef = pg->tab[pg->ordre + 1].clef;
        right->tab[0].pg = pg->tab[pg->ordre + 1].pg;
        cell->pg = right;
    }
    //No separation
    else{ 
        cell->pg = NULL;
    }
    return 1;
}

page* inserer_case(page* b_arbre, element* cell, int depth){
    if(b_arbre != NULL && cell != NULL){ 
        int pos = position(b_arbre, cell->clef);
        if(b_arbre->tab[pos].pg != NULL){
            inserer_case(b_arbre->tab[pos].pg, cell, depth +1); //Check below
            if(cell->pg != NULL){ //Might break in 2 pages, place into cell what you need
                placer(b_arbre, cell);
            }
        }
        else{
            placer(b_arbre, cell);
        }
        if(cell->pg != NULL){ //Might break in 2 pages, place into cell what you need
            if(depth == 0){ //If speparation in root, need create new page above
                page* np = new_page(b_arbre->ordre);
                np->tab[0].pg = b_arbre;
                placer(np, cell);
                return np;
            }               
        }
    }
    return b_arbre;
}

page* search(page* b_arbre, int clef){ 
    if(b_arbre != NULL && b_arbre->nb > 0){
        for(int i = 0; i <= b_arbre->nb; i++){
            if(i != 0 && clef == b_arbre->tab[i].clef){ //Found the value
                return b_arbre;
            }
            if((clef < b_arbre->tab[i+1].clef && i == 0) || (clef > b_arbre->tab[i].clef && i > 0)){ //check page last case and key
                page* for_search = search(b_arbre->tab[i].pg, clef);
                if(for_search != NULL){
                    return for_search;
                }
            }
            if(clef <b_arbre->tab[i+1].clef && i != b_arbre->nb){ //if key smaller next key, search already done
                break;
            }
        }
    } 
    return NULL;    
}

void display_RGD(page* b_arbre, int depth){
    for(int j = 0; j <(depth); j++){
        printf("        "); //Alignement 
    }
    for(int i = 1; i <= b_arbre->nb; i++){
        printf("%d ", b_arbre->tab[i].clef);      
    }
    printf("\n");
    for(int i = 0; i <= b_arbre->nb; i++){      
        if(b_arbre->tab[i].pg != NULL){
            display_RGD(b_arbre->tab[i].pg, depth+1);
        }
    }
}

void display_GRD(page* b_arbre){
    for(int i = 0; i <= b_arbre->nb; i++){
        if(i != 0){
            printf("%d\n", b_arbre->tab[i].clef);
        }        
        if(b_arbre->tab[i].pg != NULL){
            display_GRD(b_arbre->tab[i].pg);
        }
    }
}

void free_b_arbre(page* b_arbre){
    if(b_arbre != NULL){
        for(int i = 0; i < (2 * b_arbre->ordre + 1); i++){
                free_b_arbre(b_arbre->tab[i].pg);
            }
        free(b_arbre->tab);
        free(b_arbre);
    }    
}

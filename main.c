 /*
 * Author:  Vincent Steinmann
 * Date:    2021-07-15
 * 
 */
#include <stdio.h>   /* printf(), scanf() */
#include <stdlib.h>  /* srand(), rand() */
#include <time.h>    /* time() */
#include <math.h>    /* log() */
#include <stdbool.h> /* log() */
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "b_arbre.h"

int main(int argc, char const *argv[]){
    char* mode = argv[argc -2];        
    int real_argc = argc -3; // ./test_b_arbre counts as one arg and display cmd
    page* b_tree = new_page(2);
    for(int i = 0; i < real_argc; i++){ 
        int val = atoi(argv[i + 1]);//Convert string to int
        b_tree = inserer(b_tree, val);
    }
    if(strcmp(mode, "display") == 0){
        if(strcmp(argv[argc -1],"RGD") == 0){ //Last argument
            display_RGD(b_tree, 0);
        }
        else if(strcmp(argv[argc -1],"GRD") == 0){
            display_GRD(b_tree);
        }
        else{
            printf("Error display. Please use either 'RGD' or 'GRD' with display\n");
            return 0;
        }
    }
    else if(strcmp(mode, "search") == 0){ //strcmp faut return 0
        printf("%s\n", (search(b_tree, atoi(argv[argc -1]))!=NULL?"1":"0"));   
    }
    else{
        printf("Error, arguments invalid. Please use either 'display' followed by 'RGD' or 'GRD' or 'search' followed by a value \n");
        return 0;
    }
    free_b_arbre(b_tree);
    return 0;
}
